#Changelog

- Site support for bato.to [2016.11.15]
- Bug fix in comic-dl.py and added docs for readthedocs & updated readme [2016.11.20]
- Site support for kissmanga.com [2016.11.22]
- Addition of universal `downloader package` for easy maintainance [2016.11.22]
- Mangafox downloading same images fixed [2016.11.22]
- Argument priority updated [2016.11.22]